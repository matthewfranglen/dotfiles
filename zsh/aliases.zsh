alias ll='ls -l'
alias la='ls -a'
alias lal='ls -al'
alias lla='ls -al'

alias yt-dlp-album="yt-dlp --extract-audio --output '%(playlist_index)02d - %(title)s.%(ext)s'"

# vim: set filetype=zsh :
