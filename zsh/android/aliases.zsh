alias ll='ls -l'
alias la='ls -a'
alias lal='ls -al'
alias lla='ls -al'

# vim: set filetype=zsh :
