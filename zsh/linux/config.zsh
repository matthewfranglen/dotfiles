export LC_ALL="en_GB.utf8"

# https://wiki.ubuntu.com/DesktopExperienceTeam/ApplicationMenu
# Disables global menu for everything.
export UBUNTU_MENUPROXY=

# vim: set filetype=zsh :
