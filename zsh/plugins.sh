#!/usr/bin/env zsh

function install_plugins() {
    source "$("${DOTFILES}/plugin-manager/@compile-all")"

    # Set the autocomplete color for zsh-autocomplete.
    # Has to be done after loading.
    export ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE="fg=5"
}

install_plugins

unsetopt errexit
unsetopt nounset
unsetopt pipefail

unfunction install_plugins

# vim: set filetype=zsh :
