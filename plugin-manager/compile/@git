#!/usr/bin/env zsh
#
# This loads a dependency from a git repo.
# This will download the file to the xdg cache.
#
# This takes the most recent release that matches the patterns, downloads and decompresses it.
# Then it will put the root folder on the path, add any autocomplete folder to autocomplete.
#
# The commands to execute will be echoed to stdout.

set -uo pipefail

readonly PM__plugin_folder="${0:A:h:h}"
readonly DOTFILES_FOLDER="${0:A:h:h:h}"

source "${PM__plugin_folder}/lib.sh"
source "${DOTFILES_FOLDER}/script/lib.sh"

readonly URL="${1?You must provide the https url to use}"
shift

readonly FOLDER_NAME="$(pm::git_repo_folder "${URL}")"
readonly LOG_FILE="$(pm::log_file "${FOLDER_NAME}")"

(
    if [ ! -e "${FOLDER_NAME}" ]; then
        git clone "${URL}" "${FOLDER_NAME}"
    fi
) >"${LOG_FILE}.out" 2>"${LOG_FILE}.err"

LOG_LEVEL=error "${PM__plugin_folder}/@compile" directory "${FOLDER_NAME}"
success "loaded git ${URL}" >&2
