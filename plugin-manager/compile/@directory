#!/usr/bin/env zsh
#
# This loads a dependency from a local folder.
# It will put the root folder on the path, and add any autocomplete folder to autocomplete.
#
# Example:
#
# @compile directory ~/.cache/dotfiles/repos/releases/sharkdp--fd
#
# The commands to execute will be echoed to stdout.

set -uo pipefail

readonly FOLDER_NAME="${1:A}"
readonly DOTFILES_FOLDER="${0:A:h:h:h}"

source "${DOTFILES_FOLDER}/script/lib.sh"

FOUND=0
for file in "${FOLDER_NAME}"/*.plugin.zsh(N); do
    FOUND=1
    echo "source '${file}'"
done

if [ ${FOUND} -eq 0 ]; then
    if [ -e "${FOLDER_NAME}/autocomplete" ]; then
        echo 'export FPATH="${FPATH}:'"${FOLDER_NAME}/autocomplete"'"'
    fi
    if [ -n "$(find "${FOLDER_NAME}" -maxdepth 1 -type f -executable)" ]; then
        echo 'export PATH="${PATH}:'"${FOLDER_NAME}"'"'
    fi
fi
success "loaded directory ${FOLDER_NAME:t}" >&2

unset FOUND

if [ -v DEBUG ]; then
    echo "plugin::log '@directory ${FOLDER_NAME}'"
fi
