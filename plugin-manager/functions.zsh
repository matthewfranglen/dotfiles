export PM__plugin_folder="${0:A:h}"
source "${PM__plugin_folder}/variables.sh"

function pm::clear () {
    if [ -e "${PM__compiled_file}" ]; then
        rm "${PM__compiled_file}";
    fi
}

function pm::recompile () {
    pm::clear
    (
        "${DOTFILES}/plugin-manager/@compile-all" >/dev/null
    )
    echo "regenerated ${PM__compiled_file}"
}

function pm::debug () {
    pm::clear
    (
        DEBUG=1 "${DOTFILES}/plugin-manager/@compile-all" >/dev/null
    )
    echo "set commands to track time, use pm::recompile to reset"
}
