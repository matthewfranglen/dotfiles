set -euo pipefail

source "${PM__plugin_folder}/variables.sh"

function pm::git_repo_folder () {
    local URL="${1?You must provide the https url to use}"
    local NAME="${URL:gs#/#--#:gs#:##}"

    local CLONE_CACHE_FOLDER="${PM__cache_folder}/repos/clones"
    local FOLDER_NAME="${CLONE_CACHE_FOLDER}/${NAME}"

    if [ ! -e "${CLONE_CACHE_FOLDER}" ]; then
        mkdir -p "${CLONE_CACHE_FOLDER}"
    fi

    echo "${FOLDER_NAME}"
}

function pm::github_release_folder () {
    local REPO_NAME="${1?You must provide the repo to use}"
    local NAME="${REPO_NAME:gs#/#--#:gs#:##}"

    local RELEASE_CACHE_FOLDER="${PM__cache_folder}/repos/releases"
    local FOLDER_NAME="${RELEASE_CACHE_FOLDER}/${NAME}"

    if [ ! -e "${RELEASE_CACHE_FOLDER}" ]; then
        mkdir -p "${RELEASE_CACHE_FOLDER}"
    fi

    echo "${FOLDER_NAME}"
}

function pm::log_file () {
    local FOLDER_NAME="${1?You must provide the folder name to use}"
    local NAME="${FOLDER_NAME##*/}"

    if [ ! -e "${PM__log_folder}" ]; then
        mkdir -p "${PM__log_folder}"
    fi
    local LOG_FILE="${PM__log_folder}/${NAME}"

    echo "${LOG_FILE}"
}

function pm::read_tag () {
    TAG="${1?You must provide the tag to read}"
    shift

    VALUE=""
    for argument in "$@"; do
        case $argument in
            ${TAG}:*)
                VALUE="${argument/*:}"
                ;;
        esac
    done
    echo "${VALUE}"
}

function pm::install () {
    COMMAND_PREFIX=install PATH="${PM__plugin_folder}:${PATH}" "${PM__plugin_file}"
}

function pm::compile () {
    if [ -e "${PM__compiled_file}" ] && [ "${PM__compiled_file}" -nt "${PM__plugin_file:A}" ]; then
        return
    fi
    if [ ! -e "${PM__cache_folder}" ]; then
        mkdir -p "${PM__cache_folder}"
    fi

    COMMAND_PREFIX=compile PATH="${PM__plugin_folder}:${PATH}" "${PM__plugin_file}" > "${PM__compiled_file}"

    (
        autoload -U zrecompile
        local file
        for file in $PM__cache_folder/**/*.zsh{,-theme}(N); do
            zrecompile -pq "${file}"
        done
    )
}
