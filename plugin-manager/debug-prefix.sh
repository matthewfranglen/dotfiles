PLUGIN_START_TIME="$(date "+%s.%N")"
PLUGIN_LAST_TIME="${PLUGIN_START_TIME}"

function plugin::log {
    local current_time="$(date "+%s.%N")"
    debug::log "plugins" "${PLUGIN_START_TIME}" "${PLUGIN_LAST_TIME}" "${current_time}" "${1}"
    PLUGIN_LAST_TIME="${current_time}"
}

plugin::log "Starting..."

