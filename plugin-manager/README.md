Plugin Manager
==============

This is my plugin manager.
It meets my needs.
It is based on the lessons in [zsh unplugged](https://github.com/mattmc3/zsh_unplugged).

I tried several plugin managers before and they were good but lacked in one way or another.
This addresses my needs.

I'm writing this to help myself out in future.
Hello future Matt.

Usage
-----

The plugin manager runs the commands that are in "${HOME}/.zsh-plugins".
This file should be a symlink to the `zsh/zsh-plugins.symlink` file.

The commands in this file are used to install and load the plugins.
This involves two stages, installation and compilation.

Installation will set up the plugins as appropriate.
E.G. for github-releases it will download and expand the release.

Compilation writes out the commands to load the plugins to a file.
It is this file that is loaded to load your plugins.

Sources
-------

This accepts plugins from different sources.

### @directory

This loads a dependency from a local folder.
It will put the root folder on the path, and add any autocomplete folder to autocomplete.

### @dotfile

This loads a folder from within the dotfiles repo.
That involves the following:

- bin/           - anything in bin/ will get added to your path and be made
                   available anywhere.
- *.zsh          - any file ending in .zsh will get loaded into your
                   environment.
- path.zsh       - any file named path.zsh is loaded *first* and is expected to
                   setup $PATH or similar.
- completion.zsh - any file named completion.zsh is loaded *last* and is
                   expected to setup autocomplete.
- install.sh     - any file named install.sh is executed when you run
                   `script/install`, and is not loaded in the shell.
- *.symlink      - any file ending in .symlink will get symlinked to the home
                   folder (with the suffix removed) when you run
                   `script/bootstrap`, and is used to set up configuration etc.

### @git

This loads a dependency from a git repo.
This will download the file to the xdg cache.

This takes the most recent release that matches the patterns, downloads and decompresses it.
Then it will put the root folder on the path, add any autocomplete folder to autocomplete.

TODO: Support specifying version through commit-ish

### @github-release

This loads a dependency from a github release.
This will download the file to the xdg cache.

This takes the most recent release that matches the patterns, downloads and decompresses it.
Then it will put the root folder on the path, and add any autocomplete folder to autocomplete.

This uses tags to identify which release to use.
The terms: tag takes a comma separated list of words to look for in the filename of the release.
The exec: tag takes a literal command to execute after loading the release.

Example:

```
@compile github-release sharkdp/fd terms:x86_64,linux,musl
@compile github-release ajeetdsouza/zoxide terms:x86_64,linux,musl exec:'eval "$(zoxide init zsh)"'
```

### @pipx

This installs a dependency from pypi using pipx.

### @url

This installs a script from a url.

### @if

This is a conditional executor which takes a few keywords.
The subsequent command will only be executed if every keyword test passes.
The format is as follows:

@load if ssh x86_64 then directory python
@load if ! ssh x86_64 then directory python
@load if not ssh x86_64 then directory python

This will only run the @load-directory python if accessed over ssh and on a linux machine.
These are the available tests:

 * ssh - is the current connection remote (over ssh)
 * linux - is the `uname -m` x86_64
 * termux - is the `uname -m` aarch64

Since the machine architecture is static that is tested once and discarded.
The ssh connection involves wrapping the commands that would be executed.

Variables
---------

The variables that this uses are always prefixed with PM__ and most are defined in variables.sh

### PM__plugin_folder

This is NOT set in variables.sh and is required before the other variables can be defined.
It is the folder containing the plugin-manager code.

### PM__plugin_file

This is the file that is read for installation or compilation.

### PM__cache_folder

This is the folder that is used to store the compiled file as well as downloads.

### PM__log_folder

Holds the log files.

### PM__log_file

This holds the logs from installation.
Look in this file if there were problems.

### PM__compiled_file

This is the compiled form of the plugins which is loaded as your shell starts.

Functions
---------

### pm::clear

Deletes the compiled file, triggering recompilation the next time a shell is launched.

### pm::recompile

Deletes the compiled file and recompiles it.
The recompilation is without debugging.

### pm::debug

Deletes the compiled file and recompiles it.
The recompilation is with debugging.
Debugging will show the time that each applied plugin takes to load.
Use `pm::recompile` to disable debugging.

Example:

```
plugins [0.003320s Δ0.003320] - Starting...
plugins [0.006991s Δ0.003671] - @dotfile /home/matthew/Programming/Shell/dotfiles/plugin-manager
plugins [0.318176s Δ0.311185] - @dotfile /home/matthew/Programming/Shell/dotfiles/zsh/linux
plugins [0.328726s Δ0.010550] - @dotfile /home/matthew/Programming/Shell/dotfiles/zsh
plugins [0.330151s Δ0.001425] - @dotfile /home/matthew/Programming/Shell/dotfiles/fonts
plugins [0.331359s Δ0.001208] - @dotfile /home/matthew/Programming/Shell/dotfiles/git
plugins [0.389091s Δ0.057732] - @dotfile /home/matthew/Programming/Shell/dotfiles/java
plugins [0.392971s Δ0.003880] - @dotfile /home/matthew/Programming/Shell/dotfiles/javascript
plugins [0.397512s Δ0.004541] - @dotfile /home/matthew/Programming/Shell/dotfiles/kitty
plugins [0.401791s Δ0.004279] - @dotfile /home/matthew/Programming/Shell/dotfiles/postgres
plugins [0.405788s Δ0.003997] - @dotfile /home/matthew/Programming/Shell/dotfiles/powerline
plugins [0.409672s Δ0.003883] - @dotfile /home/matthew/Programming/Shell/dotfiles/puppet
plugins [0.597618s Δ0.187946] - @dotfile /home/matthew/Programming/Shell/dotfiles/python
plugins [0.601346s Δ0.003729] - @dotfile /home/matthew/Programming/Shell/dotfiles/tmux
plugins [0.605001s Δ0.003655] - @dotfile /home/matthew/Programming/Shell/dotfiles/vim
plugins [0.612954s Δ0.007952] - @directory /home/matthew/.cache/dotfiles/repos/clones/https----github.com--Tarrasch--zsh-colors
plugins [0.617308s Δ0.004354] - @directory /home/matthew/.cache/dotfiles/repos/clones/https----github.com--Tarrasch--zsh-functional
plugins [0.622226s Δ0.004919] - @directory /home/matthew/.cache/dotfiles/repos/clones/https----github.com--supercrabtree--k
plugins [0.629356s Δ0.007130] - @directory /home/matthew/.cache/dotfiles/repos/clones/https----gitlab.com--matthewfranglen--git-stashes
plugins [0.636836s Δ0.007479] - @directory /home/matthew/.cache/dotfiles/repos/clones/https----gitlab.com--matthewfranglen--random
plugins [0.644330s Δ0.007494] - @directory /home/matthew/.cache/dotfiles/repos/clones/https----github.com--CJ-Systems--gitflow-cjs
plugins [0.649027s Δ0.004697] - @directory /home/matthew/.cache/dotfiles/repos/clones/https----github.com--bobthecow--git-flow-completion
plugins [0.652483s Δ0.003456] - @directory /home/matthew/.cache/dotfiles/repos/releases/sharkdp--fd
plugins [0.655792s Δ0.003309] - @directory /home/matthew/.cache/dotfiles/repos/releases/BurntSushi--ripgrep
plugins [0.658962s Δ0.003169] - @directory /home/matthew/.cache/dotfiles/repos/releases/ajeetdsouza--zoxide
plugins [0.664641s Δ0.005679] - @directory /home/matthew/.cache/dotfiles/repos/releases/variadico--noti
plugins [0.670283s Δ0.005642] - @directory /home/matthew/.cache/dotfiles/repos/clones/https----github.com--zsh-users--zsh-autosuggestions
plugins [0.680795s Δ0.010512] - @directory /home/matthew/.cache/dotfiles/repos/clones/https----github.com--zsh-users--zsh-history-substring-search
plugins [0.697263s Δ0.016468] - @directory /home/matthew/.cache/dotfiles/repos/clones/https----github.com--zsh-users--zsh-syntax-highlighting
plugins [0.700723s Δ0.003460] - Applied plugins
```

The format is [CUMULATIVE_TIMEs ΔCOMMAND_TIME].
In the above output the `zsh/linux` dotfile took around 0.3s to load which is almost half of the total time, so optimizing that would be worthwhile.
The command times may not add up to the cumulative time as there is some housekeeping around time tracking.
