#!/usr/bin/env zsh
#
# This loads a dependency from a github release.
# This will download the file to the xdg cache.
#
# This takes the most recent release that matches the patterns, downloads and decompresses it.
#
# This uses tags to identify which release to use.
# The terms: tag takes a comma separated list of words to look for in the filename of the release.
#
# Example:
#
# @install github-release sharkdp/fd terms:x86_64,linux,musl

set -uo pipefail

readonly PM__plugin_folder="${0:A:h:h}"
readonly DOTFILES_FOLDER="${0:A:h:h:h}"

source "${PM__plugin_folder}/lib.sh"
source "${DOTFILES_FOLDER}/script/lib.sh"

readonly REPO_NAME="${1?You must provide the repo to use}"
shift

readonly FOLDER_NAME="$(pm::github_release_folder "${REPO_NAME}")"
readonly LOG_FILE="$(pm::log_file "${FOLDER_NAME}")"
readonly TERMS="$(pm::read_tag terms "$@")"
readonly EXCLUDED_TERMS="$(pm::read_tag excluded_terms "$@")"

if [ -e "${FOLDER_NAME}" ]; then
    skip "installed github release ${REPO_NAME}" >&2
else
    # needed to allow the plugin installation failure to be detected
    set +e
    (
        if [ ! -e "${FOLDER_NAME}" ]; then
            readonly TEMP_DIR="$(mktemp -d)"
            trap "rm -rf '${TEMP_DIR}'" HUP INT QUIT

            readonly RELEASE_URL="$(curl https://api.github.com/repos/${REPO_NAME}/releases/latest | python ${PM__plugin_folder}/github_release.py "--terms=${TERMS}" "--excluded-terms=${EXCLUDED_TERMS}")"
            if [ -n "${RELEASE_URL}" ]; then
                (
                    cd "${TEMP_DIR}"
                    wget "${RELEASE_URL}"
                    tar xzf *.tar.gz
                    rm *.tar.gz

                    if [ $(ls | wc -l) -gt 1 ]; then
                        mkdir "${FOLDER_NAME}"
                        mv * "${FOLDER_NAME}"
                    else
                        FILE="$(ls)"
                        if [ -d "${FILE}" ]; then
                            mv "${FILE}" "${FOLDER_NAME}"
                        else
                            mkdir "${FOLDER_NAME}"
                            mv "${FILE}" "${FOLDER_NAME}"
                        fi
                    fi
                )
            else
                false
            fi
        fi
    ) >"${LOG_FILE}.out" 2>"${LOG_FILE}.err"
    if [ $? -eq 0 ]; then
        success "installed github release ${REPO_NAME}" >&2
    else
        fail "installed github release ${REPO_NAME}" >&2
        cat "${LOG_FILE}.err" >&2
        echo >&2
    fi
    set -e
fi
