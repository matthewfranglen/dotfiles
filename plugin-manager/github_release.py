import sys
import json

def arguments():
    terms = []
    excluded_terms = []
    for argument in sys.argv[1:]:
        if argument.startswith("--terms="):
            argument = argument[len("--terms="):]
            terms = argument.split(",")
        elif argument.startswith("--excluded-terms="):
            argument = argument[len("--excluded-terms="):]
            excluded_terms = argument.split(",")
        else:
            raise ValueError("Unrecognized argument: " + argument)
    return terms, excluded_terms


def to_qualifiers(url):
    # e.g. zoxide-0.9.2-arm-unknown-linux-musleabihf.tar.gz
    name = url.split("/")[-1]
    if name.endswith(".tar.gz"):
        name = name[:-len(".tar.gz")]
    return frozenset(
        word
        for words in name.split("-")
        for word in words.split(".")
    )


terms, excluded_terms = arguments()
raw_data = sys.stdin.read()
try:
    data = json.loads(raw_data)
    urls = [
        entry["browser_download_url"]
        for entry in data["assets"]
    ]
except:
    print("failed to parse: " + raw_data)
    raise
url_qualifiers = {
    to_qualifiers(url): url
    for url in urls
}
matching_urls = [
    url
    for qualifiers, url in url_qualifiers.items()
    if all(term in qualifiers for term in terms)
    and not any(excluded_term in qualifiers for excluded_term in excluded_terms)
]

assert len(matching_urls) == 1, f"cannot resolve release, found {matching_urls} in {urls}"
print(matching_urls[0])
