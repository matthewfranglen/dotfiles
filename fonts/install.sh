#!/usr/bin/env zsh

set -eu

. "`dirname \`dirname \\\`readlink -f $0\\\`\``/script/lib.sh"

install () {
    if ! is_on_local_machine
    then
        echo "Not installing fonts... currently running on remote host" >&2
        return ${STATUS_SKIPPED}
    fi

    if ! is_font_cache_command_available
    then
        echo "Unable to install fonts... fc-cache command not found" >&2
        return ${STATUS_ERROR}
    fi

    install_fonts || return ${STATUS_ERROR}

    return ${STATUS_OK}
}

is_font_cache_command_available () {
    which fc-cache >/dev/null
}

readonly FIRA_MONO_URL="https://github.com/ryanoasis/nerd-fonts/releases/download/v3.0.2/FiraMono.zip"
readonly FONTS_FOLDER="${HOME}/.local/share/fonts"
readonly FONT_FILE="${FONTS_FOLDER}/fira-mono.zip"

install_fonts () {
    if [ ! -e "${FONTS_FOLDER}" ]; then
        mkdir -p "${FONTS_FOLDER}"
    fi
    if [ ! -e "${FONT_FILE}" ]; then
        get_url_to_file "${FIRA_MONO_URL}" "${FONT_FILE}"
    fi
    unzip -u "${FONT_FILE}" -d "${FONTS_FOLDER}" >/dev/null
    fc-cache -f "${FONTS_FOLDER}"
}

install
