#!/usr/bin/env zsh

set -eu

. "`dirname \`dirname \\\`readlink -f $0\\\`\``/script/lib.sh"

install () {
    eval "$(prepare_pyenv)" || return ${STATUS_ERROR}
    prepare_pyenv_python    || return ${STATUS_ERROR}
    prepare_pipx            || return ${STATUS_ERROR}
    install_rust            || return ${STATUS_ERROR}
    return ${STATUS_OK}
}

install_rust () {
    curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- --quiet -y --no-modify-path
}

install
