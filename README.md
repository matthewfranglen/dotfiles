dot files
=========

[![pipeline status](https://gitlab.com/matthewfranglen/dotfiles/badges/master/pipeline.svg)](https://gitlab.com/matthewfranglen/dotfiles/commits/master)

This is my current set of dotfiles. It has been rewritten to be based heavily on https://github.com/holman/dotfiles


Installation
------------

    git clone https://gitlab.com/matthewfranglen/dotfiles.git ~/.dotfiles
    cd ~/.dotfiles
    script/bootstrap
    script/install

This requires pip3 and neovim to be installed:

    sudo apt-get install python3-pip
    PIP_REQUIRE_VIRTUALENV= pip3 install neovim --user

Requirements
------------

For linux:

    sudo apt update && \
    sudo apt install \
        zsh curl wget git \
        build-essential libssl-dev zlib1g-dev libbz2-dev libreadline-dev \
        libsqlite3-dev libncursesw5-dev xz-utils tk-dev libxml2-dev \
        libxmlsec1-dev libffi-dev liblzma-dev unzip zip \
        libevent-dev bison autotools-dev automake

### TMux

This requires the latest tmux.
Download it from [github](https://github.com/tmux/tmux) and build it.
To build it to ~/.local/bin run the commands:

```
./autogen.sh
./configure --prefix ~/.local
make
make install
```

Once this is built kitty will use it.
To install the plugins after loading tmux use `ctrl-A I`.


Per Host Customization
----------------------

You can add host conditional changes with the `~/.localrc` file which will be sourced at the end.
You can add host specific plugins to `~/.local-plugins`.

Testing
-------

Yeah, it's that bad.

    docker build --tag dotfiles --no-cache .
    docker run --volume $PWD:/root/.dotfiles --rm -ti dotfiles

If successful, this will launch a zsh shell inside the docker container.
You can use this to check that all required commands are available.
